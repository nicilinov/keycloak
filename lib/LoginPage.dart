import 'package:flutter/material.dart';
import 'package:openid_client/openid_client.dart';
import 'package:openid_client/openid_client_io.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // Scaffold is a layout for the major Material Components.
    return Scaffold(
      appBar: AppBar(
        title: Text('Example title'),
      ),
      // body is the majority of the screen.
      body: Center(
        child: GestureDetector(
            onTap: authenticate(
                new Uri(
                    scheme: 'https',
                    host: 'qut.itmegastar.com',
                    path: 'keycloak/auth/realms/Checksy'),
                'public-frontend',
                []),
            child: Text('Hello, world!')),
      ),
    );
  }
}

authenticate(Uri uri, String clientId, List<String> scopes) async {
  // create the client
  var issuer = await Issuer.discover(uri);
  var client = new Client(issuer, clientId);

  // create a function to open a browser with an url
  urlLauncher(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceWebView: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  // create an authenticator
  var authenticator = new Authenticator(client,
      scopes: scopes,
      urlLancher: urlLauncher,
      redirectUri: new Uri(host: 'qut.itmegastar.com', scheme: 'https', path: 'keycloak/auth/realms/Checksy'));

  // starts the authentication
  var c = await authenticator.authorize();

  // close the webview when finished
  closeWebView();

  // return the user info
  return await c.getUserInfo();
}
